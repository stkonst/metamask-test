import { FC } from "react";
import "./ProviderInfo.css";
import { Button, Typography } from "@mui/material";
import classNames from "classnames";

type Props = {
  hasProvider: boolean | null;
  handleConnect: CallableFunction;
};

export const ProviderInfo: FC<Props> = ({ hasProvider, handleConnect }) => {
  return (
    <div className="provider-info">
      {hasProvider === null ? (
        <Typography variant="h5" className="provider-info__text">
          Checking provider...
        </Typography>
      ) : (
        <>
          <Typography
            variant="h4"
            className={classNames("provider-info__text", {
              "provider-info__text--unable": !hasProvider,
            })}
          >
            Injected Provider {hasProvider ? "DOES Exist" : "DOES NOT Exist..."}
          </Typography>
          {hasProvider ? (
            <Button className="provider-info__button" onClick={() => handleConnect()}>
              Connect MetaMask
            </Button>
          ) : (
            <Typography variant="subtitle1" className="provider-info__clue">
              Please make sure you have MetaMask browser extension installed and activated
            </Typography>
          )}
        </>
      )}
    </div>
  );
};
