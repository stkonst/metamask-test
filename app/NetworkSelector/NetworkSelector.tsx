import { forwardRef } from "react";
import { createPortal } from "react-dom";
import { Button, Grid, Typography } from "@mui/material";
import classNames from "classnames";
import { WellKnownChains } from "../types";
import "./NetworkSelector.css";

type Props = {
  setShowSelector: React.Dispatch<boolean>;
  selectedChain: string | undefined;
  setSelectedChain: CallableFunction;
};

type Ref = HTMLDivElement;

export const NetwokrSelector = forwardRef<Ref, Props>(function NetwokrSelector(
  { setShowSelector, selectedChain, setSelectedChain }: Props,
  ref,
) {
  return createPortal(
    <div ref={ref} className="selector" onClick={() => setShowSelector(false)}>
      <div className="app app--entered">
        <Grid container direction="column" className="main-grid">
          <Typography variant="h5" marginBottom={{ xs: 3, md: 5 }} color="var(--color-main)">
            Select a network
          </Typography>
          <Grid container direction="row" spacing={1}>
            {Object.entries(WellKnownChains).map(([key, val]) => {
              return (
                <Grid key={val.num} item>
                  <Button
                    className={classNames({
                      "button-selected": selectedChain === val.str,
                    })}
                    onClick={() => setSelectedChain(val.str)}
                  >
                    {key}
                  </Button>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </div>
    </div>,
    document.getElementById("portal") as HTMLDivElement,
  );
});
