import { useEffect, useState } from "react";

export const useConverter = () => {
  const [ethToBnb, setEthToBnb] = useState(1);

  useEffect(() => {
    async function updateCoeficient() {
      /*
      Crypto Converter:
      https://github.com/coinconvert/crypto-convert/blob/main/README.md
      */
      const result = await fetch("https://api.coinconvert.net/convert/eth/bnb?amount=1")
        .then((res) => res.json())
        .catch((e) => {
          console.error(e);
          return -1;
        });

      setEthToBnb(Object.values(result)[2] as number);
    }

    updateCoeficient();

    const interval = setInterval(updateCoeficient, 10000);

    return () => clearInterval(interval);
  }, []);

  return ethToBnb;
};

export const formatBalance = (rawBalance: number): number => {
  const balance = rawBalance / 1000000000000000000;
  return balance;
};

export const formatChainAsNum = (chainIdHex: string) => {
  const chainIdNum = parseInt(chainIdHex);
  return chainIdNum;
};

export const simpleAddressValidation = (wallet: string) => {
  return /^0x[0-9,a-f,A-F]{40}$/.test(wallet);
};
