import { forwardRef } from "react";
import "./ProcessingInfo.css";
import { Grid, Typography } from "@mui/material";
import { createPortal } from "react-dom";

type Props = {
  message?: string;
};

type Ref = HTMLDivElement;

export const ProcessingInfo = forwardRef<Ref, Props>(function ProcessingInfo({ message }, ref) {
  return createPortal(
    <div ref={ref} className="processing">
      <div className="app app--entered">
        <Grid container direction="column" spacing={2} className="main-grid">
          <Grid item container justifyContent="center">
            <Typography variant="h4" color="var(--color-main)">
              Processing...
            </Typography>
          </Grid>
          <Grid item container justifyContent="center">
            <Typography variant="subtitle1" color="var(--color-disabled)">
              {message}
            </Typography>
          </Grid>
        </Grid>
      </div>
    </div>,
    document.getElementById("portal") as HTMLDivElement,
  );
});
