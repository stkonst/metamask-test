export const WellKnownChains = {
  "Ethereum Mainnet": { num: 1, str: "0x1" },
  "BNB Chain": { num: 56, str: "0x38" },
  Goerli: { num: 5, str: "0x5" },
  Sepolia: { num: 11155111, str: "0xaa36a7" },
  "Linea Goerli": { num: 59140, str: "0xe704" },
};
