"use client";

import { ThemeProvider, useTheme } from "@mui/material/styles";
import { App } from "./App/App";
import { CadetBlueTheme } from "./themeProvider/CadetBlueTheme";

export default function Page() {
  const outerTheme = useTheme();
  return (
    <ThemeProvider theme={CadetBlueTheme(outerTheme)}>
      <App />
      <div id="portal"></div>
    </ThemeProvider>
  );
}
