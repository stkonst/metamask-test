import { useState, useEffect, useRef } from "react";
import classNames from "classnames";
import detectEthereumProvider from "@metamask/detect-provider";
import { MetaMaskInpageProvider } from "@metamask/providers";
import { Button, Grid, TextField } from "@mui/material";
import { CSSTransition } from "react-transition-group";
import { formatBalance, formatChainAsNum, simpleAddressValidation, useConverter } from "../utils";
import "./App.css";
import { ProviderInfo } from "../ProviderInfo/ProviderInfo";
import { NetwokrSelector } from "../NetworkSelector/NetworkSelector";
import { ProcessingInfo } from "../ProcessingInfo/ProcessingInfo";
import { WellKnownChains } from "../types";

const getChainData = require("@raiddotfarm/get-chain-name");

declare global {
  interface Window {
    ethereum?: MetaMaskInpageProvider;
  }
}

enum Chains {
  ETH = "eth",
  BNB = "bnb",
}

export const App = () => {
  const initialState: {
    accounts: string[];
    balance: number;
    chainId: string;
  } = { accounts: [], balance: 0, chainId: "" };

  const [hasProvider, setHasProvider] = useState<boolean | null>(null);
  const [wallet, setWallet] = useState(initialState);
  const [recipientAddress, setRecipientAddress] = useState("");
  const [showNetworkSelector, setShowNetworkSelector] = useState(false);
  const [selectedChain, setSelectedChain] = useState<string | undefined>();
  const [selectedChainShortName, setSelectedChainShortName] = useState("");
  const [isProcessing, setIsProcessing] = useState(false);
  const [isConnecting, setIsConnecting] = useState(false);
  const [displayEth, setDisplayEth] = useState(0);
  const [displayBnb, setDisplayBnb] = useState(0);
  const coefEthToBnb = useConverter();
  const nodeRef = useRef(null);

  useEffect(() => {
    if (wallet.chainId === "" || selectedChainShortName === "") {
      return;
    }

    setDisplayEth(displayBalance(Chains.ETH));
    setDisplayBnb(displayBalance(Chains.BNB));
  }, [wallet, selectedChainShortName, coefEthToBnb]);

  const displayBalance = (chain: Chains) => {
    if (["eth", "gor", "sep", "lin"].includes(selectedChainShortName)) {
      if (chain === Chains.ETH) {
        return wallet.balance;
      } else if (chain === Chains.BNB) {
        return wallet.balance * coefEthToBnb;
      }
    }

    if (selectedChainShortName === "bnb") {
      if (chain === Chains.ETH) {
        return wallet.balance / coefEthToBnb;
      } else if (chain === Chains.BNB) {
        return wallet.balance;
      }
    }

    return -1;
  };

  const applyShortName = (chainId: string) => {
    const chainIdNumber = formatChainAsNum(chainId);
    const chainData = getChainData(chainIdNumber, true);
    const shortName = chainData.shortName.slice(0, 3);
    setSelectedChainShortName(shortName);
  };

  const refreshAccounts = (accounts: any) => {
    if (accounts.length > 0) {
      updateWallet(accounts);
    } else {
      setWallet(initialState);
    }
  };

  const refreshChain = (chainId: any) => {
    setWallet((wallet) => ({ ...wallet, chainId }));
    handleConnect();

    const chain = Object.entries(WellKnownChains).find(([_, val]) => {
      return val.str === chainId;
    });
    if (chain) {
      setSelectedChain(chain[1].str);
      applyShortName(chain[1].str);
    }
  };

  const updateWallet = async (accounts: any) => {
    const chainId = (await window.ethereum!.request({
      method: "eth_chainId",
    })) as string;

    const chain = Object.entries(WellKnownChains).find(([_, val]) => {
      return val.str === chainId;
    });
    if (chain) {
      setSelectedChain(chain[1].str);
      applyShortName(chain[1].str);
    }

    const balance = formatBalance(
      parseInt(
        (await window.ethereum!.request({
          method: "eth_getBalance",
          params: [accounts[0], "latest"],
        })) as string,
      ),
    );

    setWallet({ accounts, balance, chainId });
  };

  useEffect(() => {
    const getProvider = async () => {
      const provider = await detectEthereumProvider({ silent: true });
      setHasProvider(Boolean(provider));

      if (provider) {
        const accounts = await window.ethereum?.request({
          method: "eth_accounts",
        });
        refreshAccounts(accounts);
        window.ethereum?.on("accountsChanged", refreshAccounts);
        window.ethereum?.on("chainChanged", refreshChain);
      }
    };

    getProvider();

    return () => {
      window.ethereum?.removeListener("accountsChanged", refreshAccounts);
      window.ethereum?.removeListener("chainChanged", refreshChain);
    };
  }, []);

  const handleConnect = async () => {
    let accounts = await window.ethereum?.request({
      method: "eth_requestAccounts",
    });
    updateWallet(accounts);
  };

  const changeChainClickHandler = () => {
    setShowNetworkSelector(true);
  };

  const changeChainHandler = (chainId: string) => {
    setIsProcessing(true);

    (async () => {
      await window.ethereum!.request({
        method: "wallet_switchEthereumChain",
        params: [
          {
            chainId,
          },
        ],
      });
    })()
      .then(() => {
        setSelectedChain(chainId);
        applyShortName(chainId);
        handleConnect();
      })
      .catch((e: any) => console.error(e.message))
      .finally(() => setIsProcessing(false));
  };

  return (
    <div
      className={classNames("app", {
        "app--entered": wallet.accounts.length > 0,
      })}
    >
      <CSSTransition
        nodeRef={nodeRef}
        in={isProcessing}
        mountOnEnter
        unmountOnExit
        timeout={500}
        classNames="processing"
      >
        <ProcessingInfo ref={nodeRef} message="waiting for the confirmation from user" />
      </CSSTransition>

      {isConnecting ? (
        <ProcessingInfo ref={nodeRef} message="connecting..." />
      ) : wallet.accounts.length === 0 ? (
        <ProviderInfo hasProvider={hasProvider} handleConnect={handleConnect} />
      ) : (
        <Grid container justifyContent="center" alignContent="start" spacing={2} className="main-grid">
          <Grid item xs={12}>
            <TextField
              variant="standard"
              multiline
              maxRows={2}
              disabled
              fullWidth
              size="small"
              label="Wallet Accounts"
              value={wallet.accounts[0]}
              className="wallet-number"
            />
          </Grid>

          <Grid item container xs={12} justifyContent="space-between">
            <Grid item xs={3}>
              <Button className="button-change-currency" onClick={changeChainClickHandler}>
                {selectedChainShortName}
              </Button>

              <CSSTransition
                nodeRef={nodeRef}
                in={showNetworkSelector}
                mountOnEnter
                unmountOnExit
                timeout={300}
                classNames="selector"
              >
                <NetwokrSelector
                  ref={nodeRef}
                  setShowSelector={setShowNetworkSelector}
                  selectedChain={selectedChain}
                  setSelectedChain={changeChainHandler}
                />
              </CSSTransition>
            </Grid>

            <Grid item xs={4}>
              <TextField
                fullWidth
                InputProps={{
                  readOnly: true,
                }}
                label="ETH"
                value={displayEth.toFixed(3)}
              />
            </Grid>

            <Grid item xs={4}>
              <TextField
                fullWidth
                InputProps={{
                  readOnly: true,
                }}
                label="BNB"
                value={displayBnb.toFixed(3)}
              />
            </Grid>
          </Grid>

          <Grid item xs={12} sm={9}>
            <TextField
              fullWidth
              multiline
              maxRows={4}
              label="recipient wallet number"
              className="wallet-number"
              value={recipientAddress}
              onChange={(e) => setRecipientAddress(e.currentTarget.value)}
            />
          </Grid>

          <Grid item container xs={12} sm={3} justifyContent="end">
            <Button
              disabled={!simpleAddressValidation(recipientAddress)}
              className={classNames("button-send", {
                "button-send--disabled": !simpleAddressValidation(recipientAddress),
              })}
              onClick={() => null}
            >
              Send
            </Button>
          </Grid>
        </Grid>
      )}
    </div>
  );
};
