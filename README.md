# MetaMask Test

### Test application to connect to MetaMask provider.
It displays your wallet number, balance in ETH and BNB.

To get started first make sure you have installed and activated MetaMask provider. The application will prompt you in case if there is some problems with connection.

<img src="img/1.png" width=150 />
<img src="img/2.png" width=150 />

If you connect to MetaMask for the first time you have to confirm connection by pushing the button.


After confirmation on MetaMask browser extension side, you can see you wallet number and other information.

<img src="img/3.png" width=150 />
<img src="img/4.png" width=150 />

There is a function to switch the active chain as well.
